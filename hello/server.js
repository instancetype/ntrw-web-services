#!/usr/bin/env node --harmony
'use strict'

const
  express = require('express')
, app = express()

app
  .use(express.logger('dev'))
  .get('/api/:name', function(req, res) {
         res.json(200, { 'hello' : req.params.name })
       })
  .listen(3000, function() {
            console.log('App is ready...')
          })
